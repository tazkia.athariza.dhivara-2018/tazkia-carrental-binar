# Binar Car Rental
"Binar Car Rental" was created for academic purpose, namely Challenge Chapter 3 in Binar Academy - Fullstack Web Course.

## About
This website contains 6 section (excluding nav-bar and footer) and built by :
- HTML5
- CSS
- Bootstrap v5.0.2 (online ver)

## Getting Started
You can run the program in your browser with internet connection. These website pages are already suitable and responsive for desktop and mobile display. 

## Version History
Commit changes can be checked on https://gitlab.com/tazkia.athariza.dhivara-2018/tazkia-carrental-binar

## Author
Tazkia Athariza Dhivara